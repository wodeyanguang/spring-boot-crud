package all.service.impl;

import all.mapper.EmpMapper;
import all.pojo.Emp;
import all.service.EmpService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author ：Yan Guang
 * @date ：Created in 2020/11/12 13:29
 * @description：
 */
@Service
public class EmpServiceImpl implements EmpService {

    @Autowired
    private EmpMapper empMapper;

    @Override
    public void insert(Emp emp) {
        empMapper.insert(emp);
    }

    @Override
    public Emp getById(int empId) {
        Emp emp = empMapper.getById(empId);
        return emp;
    }

    @Override
    public List<Emp> list() {
        List<Emp> list = empMapper.list();
        return list;
    }

    @Override
    public void update(Emp emp) {
        empMapper.update(emp);
    }

    @Override
    public void delete(int empId) {
        empMapper.delete(empId);
    }
}
