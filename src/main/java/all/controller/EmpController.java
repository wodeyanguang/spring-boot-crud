package all.controller;

import all.pojo.Emp;
import all.service.EmpService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author ：Yan Guang
 * @date ：Created in 2020/11/12 13:32
 * @description：
 */
@Controller
public class EmpController {
    @Autowired
    private EmpService empService;

    @PostMapping("insert")
    public String insert(Emp emp) {
        empService.insert(emp);
        return "main";
    }

    @GetMapping("getById")
    public Emp getById(int empId) {
        Emp emp = empService.getById(empId);
        return emp;
    }

    @GetMapping("main")
    public String list(Model model) {
        List<Emp> list = empService.list();
        model.addAttribute("list",list);
        return "main";
    }

    @PostMapping("update")
    public void update(Emp emp) {
        empService.update(emp);
    }

    @GetMapping("delete")
    public void delete(int empId) {
        empService.delete(empId);
    }
}
