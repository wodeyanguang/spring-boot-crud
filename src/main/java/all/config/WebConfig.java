package all.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * @author ：Yan Guang
 * @date ：Created in 2020/11/12 14:43
 * @description：
 */
@Configuration
public class WebConfig implements WebMvcConfigurer {

    @Override
    public void addViewControllers(ViewControllerRegistry registry) {
        //这里相当于视图解析器的作用,会自动去寻找templates下面的
        //这里就相当于我们自己手动配置页面跳转了，会跳转到我们的templates路径下的资源
        registry.addViewController("/index").setViewName("index");
        registry.addViewController("/header").setViewName("header");
        //这个地方我们需要通过index先静态加载(src的方式)我们的controller里面的main
        // 然后再通过springboot自带的视图解析器然后到templates下面的main里面去
        //registry.addViewController("/main").setViewName("main");
        registry.addViewController("/menu").setViewName("menu");
        registry.addViewController("/toSave").setViewName("save");
    }
}
