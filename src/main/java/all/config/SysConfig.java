package all.config;

import com.alibaba.druid.pool.DruidDataSource;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.sql.DataSource;

/**
 * @author ：Yan Guang
 * @date ：Created in 2020/11/12 13:14
 * @description：
 */
@Configuration
public class SysConfig {

    //配置这个我们就可以在yml中写入我们自己定义的德鲁伊的数据源的相关配置，也就是yml中黄色的那些配置
    @Bean
    @ConfigurationProperties("spring.datasource")
    public DataSource dataSource(){
        return new DruidDataSource();
    }
}
