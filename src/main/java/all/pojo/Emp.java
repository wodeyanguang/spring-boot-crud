package all.pojo;

import lombok.Data;
import org.springframework.context.annotation.Bean;

import java.util.Date;

/**
 * @author ：Yan Guang
 * @date ：Created in 2020/11/12 13:22
 * @description：
 */

@Data
public class Emp {
    private int pid;
    private String username;
    private String password;
    private int gender;
    private String pAddr;//数据库表里面是p_Addr
    private Date birth;

    public int getPid() {
        return pid;
    }

    public void setPid(int pid) {
        this.pid = pid;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getGender() {
        return gender;
    }

    public void setGender(int gender) {
        this.gender = gender;
    }

    public String getpAddr() {
        return pAddr;
    }

    public void setpAddr(String pAddr) {
        this.pAddr = pAddr;
    }

    public Date getBirth() {
        return birth;
    }

    public void setBirth(Date birth) {
        this.birth = birth;
    }
}
